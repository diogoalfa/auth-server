create table user
(
  id    bigint auto_increment primary key,
  client_id  varchar(255) null,
  first_name varchar(255) null,
  last_name  varchar(255) null,
  password   varchar(255) null,
  salary     bigint       null,
  username   varchar(255) null
);


insert into user (client_id,first_name,last_name,password,salary,username)
values ('diego_clientId','Diego','Navia','$2a$10$po4py1P/MTYr6K6NGQWwGOqXcmb4wwPx1BU8D2rItuQx.ppFG5vJy',1000000000,'diego');

