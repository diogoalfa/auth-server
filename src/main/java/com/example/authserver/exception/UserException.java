package com.example.authserver.exception;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public class UserException extends RuntimeException {

    public UserException(String message) {
        super(message);
    }
}
