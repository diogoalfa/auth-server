package com.example.authserver.service;

import com.example.authserver.model.User;


/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public interface UserService {

    /**
     *
     * @param id
     * @return
     */
    public User findUserById(long id);

    /**
     *
     * @param username
     * @param pass
     * @return
     */
    public User findUserByIdUsernameAndPass(String username,String pass);

}
