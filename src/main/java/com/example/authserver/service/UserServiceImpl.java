package com.example.authserver.service;

import com.example.authserver.model.User;
import com.example.authserver.model.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService,UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);


    @Autowired
    private UserDao userDao;


    @Override
    public User findUserById(long id) {
        return null;
    }

    @Override
    public User findUserByIdUsernameAndPass(String username, String pass) {
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String s)  {
        LOGGER.debug("loadUserByUsername:{}",s);
        User user = userDao.findByUsername(s);
        LOGGER.debug("userDAO:{}",user);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                getAuthority()
        );
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("USER"));
    }


}
