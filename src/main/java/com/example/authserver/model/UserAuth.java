package com.example.authserver.model;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
public class UserAuth {

    private String username;
    private String pass;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
