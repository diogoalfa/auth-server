package com.example.authserver.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Diego Navia <diego.navia@icloud.com>
 * @file-description :
 */
@Repository
public interface UserDao extends CrudRepository<User, Long> {

    @Query("select u from User u where u.username = ?1")
    User findByUsername(String username);

    @Query("select u from User u where u.clientId = ?1")
    User findByUserClientId(String clientId);

    @Query("select u from User u where u.username = ?1 and u.password= ?2")
    User findUserByUsernameAndPassword(String username, String pass);
}
