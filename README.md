## Tecnologías 

* sprint boot v2.1.5.RELEASE
* java 1.8
* mysql 8.0.17

# Lanzamiento

1. Crear Base datos : 

Nombre:test1

Host:mysql://localhost:3306/test1

2. ejecutar script :

link > https://gitlab.com/diogoalfa/auth-server/-/blob/master/doc/db/script-inicial.sql

3. ejecutar en consola : mvn spring-boot:run


### Endpoint

1. Obtener Token:

* http://localhost:8080/oa/oauth/token
* method: POST
* Basic :

1.Username: exampleClient

2.Password: exampleSecret

* BodyRequest: x-wwww-form-urlencoded
```
username:diego
grant_type:password
password:clave123
```